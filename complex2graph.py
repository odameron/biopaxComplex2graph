#! /usr/bin/env python3

import argparse
import graphviz
import json
import pathlib
import rdflib
from SPARQLWrapper import SPARQLWrapper, JSON
 
import uri_utils


#endpointURL = 'http://localhost:3030/reactome/query'
#complexIdent = 'reactome:Complex1'
#complexIdent = 'reactome:Complex10'
#complexIdent = 'reactome:Complex100'

# Complex9348 and Complex9364 have exactly the same components, but with different stoichiometric coefficients
#complexIdent = 'reactome:Complex9348'
#complexIdent = 'reactome:Complex9364'

# Complex6573 and Complex6610 have exactly the same components with the same stoichiometric coefficients
#complexIdent = 'reactome:Complex6573'
#complexIdent = 'reactome:Complex6610'


def getEntityDisplayNameStableIdentifier(dataSource, entityIdent, prefixesDict={}):
	"""
	Compute a dictionnary describing an entity.

	Parameters
	----------
	dataSource : str
		Either the URL of the SPARQL endpoint or the path of the file.
	entityIdent : str
		Identifier (either CURIe or full URI) of the entity.
	prefixesDict : dict of {str : str}, default {}
		Prefixes names and values.

	Returns
	-------
	dictEntityDescription : {'entity':???, 'entityName':???, 'entityIdent':???}
		Dictionnary describing an entity.
	"""
	queryPath = 'queries/template-entityReactomeIdentifier.rq'
	sparqlQuery = pathlib.Path(queryPath).read_text().replace('$supplementaryPrefixes$',uri_utils.convertPrefixesDictToSPARQL(prefixesDict))
	if entityIdent.startswith('http'):
		sparqlQuery = sparqlQuery.replace('$entityValue$','<' + entityIdent + '>')
	else:
		sparqlQuery = sparqlQuery.replace('$entityValue$',entityIdent)

	dictEntityDescription = {}
	if dataSource.startswith("http"):
		sparql = SPARQLWrapper(dataSource)
		sparql.setQuery(sparqlQuery)
		sparql.setReturnFormat(JSON)
		results = sparql.query().convert()
		for result in results["results"]["bindings"]:
			dictEntityDescription['entity'] = uri_utils.getFullURI(result['entity']['value'], prefixesDict)
			dictEntityDescription['entityName'] = result['entityName']['value']
			if dictEntityDescription['entityName'] == None:
				dictEntityDescription['entityName'] = ""
			dictEntityDescription['entityIdent'] = result['entityIdent']['value']
			if dictEntityDescription['entityIdent'] == None:
				dictEntityDescription['entityIdent'] = ""
	else:
		# TODO: guess format from file extension
		g = rdflib.Graph().parse(dataSource, format="text/turtle")
		for result in g.query(sparqlQuery):
			dictEntityDescription['entity'] = uri_utils.getFullURI(result['entity'], prefixesDict)
			dictEntityDescription['entityName'] = result['entityName']
			dictEntityDescription['entityIdent'] = result['entityIdent']
			if dictEntityDescription['entityName'] == None:
				dictEntityDescription['entityName'] = ""
			if dictEntityDescription['entityIdent'] == None:
				dictEntityDescription['entityIdent'] = ""
	return dictEntityDescription


def complex2graph(dataSource, complexIdent, prefixesDict={}, dotFilePath=""):
	queryPath = 'queries/template-complexComponentsHierarchyWithStoichiometricCoefficient.rq'
	sparqlQuery = pathlib.Path(queryPath).read_text().replace('$supplementaryPrefixes$',uri_utils.convertPrefixesDictToSPARQL(prefixesDict))
	if complexIdent.startswith('http'):
		sparqlQuery = sparqlQuery.replace('$complexValue$','<' + complexIdent + '>')
	else:
		sparqlQuery = sparqlQuery.replace('$complexValue$',complexIdent)

	schemaGraph = graphviz.Digraph('G')
	schemaGraph.attr(rankdir="BT")

	solutions = []
	vertices = set()
	if dataSource.startswith("http"):
		sparql = SPARQLWrapper(dataSource)
		sparql.setQuery(sparqlQuery)
		sparql.setReturnFormat(JSON)
		results = sparql.query().convert()
		for result in results["results"]["bindings"]:
			currentSolution = {}
			currentSolution['comp'] = uri_utils.getLocalName(result['comp']['value'], prefixesDict)
			currentSolution['complexPart'] = uri_utils.getLocalName(result['complexPart']['value'], prefixesDict)
			if 'stoichioValue' in result.keys():
				currentSolution['stoichioValue'] = float(result['stoichioValue']['value'])
			else:
				currentSolution['stoichioValue'] = 1.0
			solutions.append(currentSolution)
			vertices.add(result['comp']['value'])
			vertices.add(result['complexPart']['value'])
	else:
		g = rdflib.Graph().parse(dataSource, format="text/turtle")
		for result in g.query(sparqlQuery):
			currentSolution = {}
			currentSolution['comp'] = uri_utils.getLocalName(result['comp'], prefixesDict)
			currentSolution['complexPart'] = uri_utils.getLocalName(result['complexPart'], prefixesDict)
			currentSolution['stoichioValue'] = float(result['stoichioValue'])
			solutions.append(currentSolution)
			vertices.add(str(result['comp']))
			vertices.add(str(result['complexPart']))

	for currentSolution in solutions:
		if (currentSolution['stoichioValue']).is_integer():
			schemaGraph.edge(currentSolution['comp'], currentSolution['complexPart'], arrowhead='diamond', label=str(int(currentSolution['stoichioValue'])))
		else:
			schemaGraph.edge(currentSolution['comp'], currentSolution['complexPart'], arrowhead='diamond', label=str(currentSolution['stoichioValue']))
	for currentVertex in vertices:
		currentVertexDescr = getEntityDisplayNameStableIdentifier(dataSource, str(currentVertex), prefixesDict)
		schemaGraph.node(uri_utils.getLocalName(currentVertex, prefixesDict), "{}\n{}\n({})".format(currentVertexDescr['entityName'], currentVertexDescr['entityIdent'], str(uri_utils.getLocalName(currentVertex, prefixesDict))))

	if dotFilePath == "":
		dotFilePath = complexIdentifierToDotFileName(complexIdent)
	schemaGraph.save(filename=dotFilePath)



def complexIdentifierToDotFileName(complexIdent):
	sep = max(0, complexIdent.rfind('/')+1, complexIdent.rfind('#')+1, complexIdent.rfind(':')+1)
	return complexIdent[sep:]+'.dot'



def getConversionToImageCommand(dotFilePath):
	return "convert to png with: dot -Tpng " + dotFilePath + " -o " + dotFilePath[:-3]+'png'





if __name__ == "__main__":
	# 
	# ./complex2graph.py http://localhost:3030/reactome/query reactome:Complex1 defaultPrefixes.json
	#
	# ./complex2graph.py Complex10-valid.ttl reactome:Complex10 defaultPrefixes.json Complex10-valid.dot
	parser = argparse.ArgumentParser(description='Generate a graphviz representation (in .dot format) of a BioPAX-level3 molecular complex')
	parser.add_argument("dataSource", help="either URL (starting with 'http') of the SPARQL endpoint having molecular complexes in BioPAX-level3 format, or file path (starting with anything but 'http') of the RDF file")	# 'http://localhost:3030/reactome/query'
	parser.add_argument("complexIdentifier", help="identifier of the molecular complex (CURIE or full URI)")	# 'reactome:Complex1'
	parser.add_argument("jsonPrefixesFilePath", help="path for the json file containing a dictionnary which keys are prefixes and associated values are URIs (default: empty)", nargs='?', default="")	# defaultPrefixes.json
	parser.add_argument("dotFilePath", help="path for the .dot file to be generated (default: guess from complexIdentifier)", nargs='?', default="")
	args = parser.parse_args()
	prefixesDict = {}
	if args.jsonPrefixesFilePath != "":
		prefixesDict = uri_utils.readPrefixesFromFile(args.jsonPrefixesFilePath)
	dotFilePath = args.dotFilePath
	if dotFilePath == "":
		dotFilePath = complexIdentifierToDotFileName(args.complexIdentifier) 
	complex2graph(args.dataSource, args.complexIdentifier, prefixesDict=prefixesDict, dotFilePath=dotFilePath)
	print("  " + getConversionToImageCommand(dotFilePath))

