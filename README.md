# biopaxComplex2graph

Generates a graph-like representation of a molecular complex in BioPAX


# Dependencies

- graphviz
- sparqlwrapper


# Usage

```bash
# query a SPARQL endpoint
# generates a dot file (Complex1.dot) file based on the local part or the complex identifier
python3 complex2graph.py http://localhost:3030/reactome/query reactome:Complex1 defaultPrefixes.json

# query a BioPAX-v3 file (Complex10-valid.ttl)
# generates a dot file (Complex10-valid.dot) based on the optional argument
python3 complex2graph.py Complex10-valid.ttl reactome:Complex10 defaultPrefixes.json Complex10-valid.dot
```

generates a `.dot` file that can be converted into an image.
For example, `reactome:Complex10` in Reactome-Human gives ([.dot](./img/Complex10.dot), [.png](./img/Complex10.png))

![Schema of the reactome:Complex10 components in Homo sapiens](./img/Complex10.png "Complex10 components in Homo sapiens")


# Todo

- [ ] add docstrings for functions
    - [ ] file `complex2graph.py`
    - [x] file `complexComponents.py`
- [x] remove `reactome` prefix from SPARQL queries
- [x] better prefix handling
    - [x] add a function for reading the default prefixes from a json file
    - [x] add an empty {(prefixName, prefixValue)} dictionnary as an optional argument to functions
- [ ] remove `reactome` prefix and URI from python code
    - [x] function generatePrefixedIdentifier(...)
    - [x] function getLocalName(...)
        - [ ] FIXME: no need for prefixesDict argument
- [x] support getting the data from an endpoint or from a file
    - [x] rename the `endpointURL` argument as `dataSource`
    - [x] if `dataSource` starts with `http`, assume endpoint, otherwise assume filepath
    - [ ] if filepath, guess the format from the extension
- [x] take complex identifier as an argument
- [ ] use `bp3:displayName`
- [ ] use stable identifiers with `bp3:xref/bp3:id`
- [ ] file `complexComponents.py`
    - [ ] generate `.dot` representation: `getRepresentationDot(dotFilePath="")`
    - [ ] generate BioPAX representation
        - [x] function `getRepresentationBiopaxOriginal(biopaxFilePath="")`
        - [x] function `getRepresentationBiopaxValid(biopaxFilePath="")`
            - [x] retrieve all the triples for which the property is not `bp3:component`
            - [x] generate the `bp3:component` triples from the result of `getComponentsStoichiometry(...)`
        - [ ] generate graphical representation of the BioPAX (not just of the components)
    - [ ] update the Usage section of `README.md` with these functions
- [ ] generate `owl:sameAs` relations between redundant complexes
    - [ ] file `reactome-v77-redundantComplexes-orig-mappings.ttl`
    - [ ] file `reactome-v77-redundantComplexes-valid-mappings.ttl`


